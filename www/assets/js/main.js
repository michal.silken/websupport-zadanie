window.onload = function () {
    var popupContainer = document.querySelector('[data-dynamic-popup-container]');
    var elementsToAdd = document.querySelectorAll('[data-record-add]');
    var elementsToRemove = document.querySelectorAll('[data-record-remove]');
    var popupElement = document.querySelector('[data-popup]');

    elementsToAdd.forEach(function (element, i) {
        element.addEventListener('click', function () {
            openPopupForAdd(this, popupContainer);
        });
    });

    elementsToRemove.forEach(function (element, i) {
        element.addEventListener('click', function () {
            openPopupForRemove(this, popupElement);
        })
    });
};

var openPopupForAdd = function (element, popupContainer) {
    var type = element.getAttribute('data-record-add');

    var request = new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        var params = 'getPopupTemplate=' + type;
        xhr.open('POST', '?api', true);
        xhr.onload = function() {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject();
            }
        };
        xhr.send(params);
    });

    request.then(function (value) {
        popupContainer.innerHTML = value.content.trim();
        var popupElement = popupContainer.firstChild;

        var popupOptions = {
            buttons: {
                cancel: {
                    callback: function (callback) {
                        callback();
                    }
                },
                confirm: {
                    callback: function (callback) {

                        var form = popupElement.querySelector('[data-popup-form]');
                        var formData = new FormData(form);
                        var data = [
                            'create=1',
                            'type=' + type
                        ];
                        for(var [key,value] of formData.entries()) {
                            data.push(key + '=' + value);
                        }
                        var params = data.join('&');

                        var request = new Promise(function(resolve, reject) {
                            var xhr = new XMLHttpRequest();
                            xhr.open('POST', '?api', true);
                            xhr.onload = function() {
                                if (this.status >= 200 && this.status < 300) {
                                    resolve(xhr.response);
                                } else {
                                    reject();
                                }
                            }
                            xhr.send(params);
                        });

                        request.then(function (value) {
                            var data = JSON.parse(value);
                            if (data.status === true) {
                                callback();
                                location.reload();
                            }

                            if (data.errors) {
                                for (var [fieldName, message] of Object.entries(data.errors)) {
                                    var errorElement = form.querySelector('[data-error="' + fieldName + '"]');
                                    if (errorElement) {
                                        errorElement.innerHTML = message;
                                    }
                                }
                            }
                            callback();
                        }).catch(function () {
                            callback();
                        });
                    }
                }
            }
        };

        var popup = new Popup(popupElement, popupOptions);
        popup.open();

    });
};

var openPopupForRemove = function (element, popupElement) {
    var id = element.getAttribute('data-record-remove');
    var type = element.getAttribute('data-record-type');
    var popupOptions = {
        title: 'Naozaj chcete vymazať ' + type,
        content: 'ID: ' + id,
        buttons: {
            cancel: {
                name: 'Nie',
                callback: function (callback) {
                    callback();
                }
            },
            confirm: {
                name: 'Áno',
                callback: function (callback) {
                    var request = new Promise(function(resolve, reject) {
                        var xhr = new XMLHttpRequest();
                        var params = 'removeId=' + id;
                        xhr.open('POST', '?api', true);
                        xhr.onload = function() {
                            if (this.status >= 200 && this.status < 300) {
                                resolve(xhr.response);
                            } else {
                                reject();
                            }
                        }
                        xhr.send(params);
                    });

                    request.then(function (value) {
                        callback();
                        location.reload();
                    }).catch(function () {
                        alert('Záznam sa nepodarilo odstrániť!');
                    });
                }
            }
        }
    };
    var popup = new Popup(popupElement, popupOptions);
    popup.open();
};

var Popup = function (element, options) {

    var _this = {};

    _this.element = element;

    _this.bodyElement = document.getElementsByTagName('body')[0];

    _this.popup = {
        title: _this.element.querySelector('[data-popup-title]'),
        content: _this.element.querySelector('[data-popup-content]'),
        buttons: {
            cancel: _this.element.querySelector('[data-button-cancel]'),
            confirm: _this.element.querySelector('[data-button-confirm]')
        },
        loading: _this.element.querySelector('[data-loading]'),
        overlay: _this.element.querySelector('[data-popup-overlay]')
    };

    _this.defaultOptions = {
        title: null,
        content: null,
        buttons: {
            cancel: {
                name: null,
                callback: function (callback) {
                    callback();
                }
            },
            confirm: {
                name: null,
                callback: function (callback) {
                    callback();
                }
            }
        }
    };

    _this.options = Object.assign(_this.defaultOptions, options || {});

    var init = function () {

        _this.reset();

        _this.popup.overlay.addEventListener('click', function () {
            _this.close();
        });
        element.querySelector('[data-popup-close]').addEventListener('click', function () {
            _this.close();
        });
        // confirm popup after click on confirm button
        _this.popup.buttons.confirm.addEventListener('click', function () {
            _this.disableButtons();
            _this.showLoading();
            _this.clearErrors();
            _this.options.buttons.confirm.callback(function (status) {
                if (!status) {
                    _this.hideLoading();
                    _this.enableButtons();
                }
            })
        });
        // close popup after click on cancel button
        _this.popup.buttons.cancel.addEventListener('click', function () {
            _this.options.buttons.cancel.callback(function () {
                _this.close();
            })
        });
    };

    _this.open = function()
    {
        _this.disableScrolling();
        _this.element.classList.add('popup--show');
    };

    _this.close = function () {
        _this.element.classList.remove('popup--show');
        _this.enableScrolling();
    };

    _this.reset = function () {
        // set popup content by options
        if (_this.options.title) {
            _this.popup.title.innerHTML = _this.options.title;
        }
        if (_this.options.content) {
            _this.popup.content.innerHTML = _this.options.content;
        }
        if (_this.options.buttons.cancel.name) {
            _this.popup.buttons.cancel.innerHTML = _this.options.buttons.cancel.name;
        }
        if (_this.options.buttons.confirm.name) {
            _this.popup.buttons.confirm.innerHTML = _this.options.buttons.confirm.name;
        }

        _this.disableScrolling();
        _this.hideLoading();
        _this.enableButtons();
    };

    _this.showLoading = function() {
        var loadingClassToShow = _this.popup.loading.getAttribute('data-loading');
        _this.popup.loading.classList.add(loadingClassToShow);
    };

    _this.hideLoading = function() {
        var loadingClassToShow = _this.popup.loading.getAttribute('data-loading');
        _this.popup.loading.classList.remove(loadingClassToShow);
    };

    _this.disableScrolling = function () {
        _this.bodyElement.classList.add('no-scroll');
    };

    _this.enableScrolling = function () {
        _this.bodyElement.classList.remove('no-scroll');
    };

    _this.disableButtons = function () {
        for (var button in _this.popup.buttons) {
            _this.popup.buttons[button].setAttribute('disabled', 'disabled');
        }
    };

    _this.enableButtons = function () {
        for (const button in _this.popup.buttons) {
            _this.popup.buttons[button].removeAttribute('disabled');
        }
    };

    _this.clearErrors = function () {
        var errorsElements = _this.element.querySelectorAll('[data-error]');
        for (var [key,el] of errorsElements.entries()) {
            el.innerHTML = '';
        }
    };

    this.open = _this.open;

    init();
};