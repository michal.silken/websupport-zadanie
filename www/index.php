<?php

require_once '../autoload.php';

use classes\API\RequestResolver;
use classes\API\requestTypes\GetPopupTemplate;
use classes\API\requestTypes\Remove;
use classes\API\requestTypes\Create;


$apiKey = 'df1e7575-b0e7-4e49-888c-8f7165e4e344';
$secret = '4f6984f6486f27edf2ce466f976859399cc466a5';

$domain = 'php-assignment.eu';

$connector = new \classes\websupport\api\Connector($apiKey, $secret);
$manager = new \classes\websupport\api\Manager($connector);

if (isset($_GET['api'])) {
    header('Content-Type: application/json');

    $postdata = file_get_contents('php://input');
    parse_str($postdata, $post);

    $resolver = new RequestResolver($post);
    $resolver->add(new GetPopupTemplate());
    $resolver->add(new Create($manager, $domain));
    $resolver->add(new Remove($manager, $domain));
    echo json_encode($resolver->resolve());
    exit;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
        <link href="./assets/css/style.css" rel="stylesheet">
        <script src="./assets/js/main.js"></script>
    </head>
    <body style="no-scroll">
            <div class="container">
                <h1>Zadanie</h1>
                <?php
                    $response = $manager->getListOfAllZones($domain);
                    foreach ($manager->getSupportedRecordTypes() as $recordType) {
                        $data = $response[$recordType] ?? null;
                        if ($data === null) {
                            ?>
                            <table class="table table--main">
                                <th><?=$recordType?> záznamy</th>
                                <th class="text-right" colspan="3">
                                    <button data-record-add="<?=$recordType?>" class="button button--confirm">Vytvoriť nový záznam</button>
                                </th>
                            </table>
                            <?php
                            continue;
                        }
                        ?>
                        <table class="table table--main">
                            <tr>
                                <th colspan="<?=count($data->getColNames())-2?>"><strong><?=$recordType?> záznamy</strong></th>
                                <th class="text-right" colspan="3">
                                    <button data-record-add="<?=$recordType?>" class="button button--confirm">Vytvoriť nový záznam</button>
                                </th>
                            </tr>
                            <tr>
                                <?php
                                    foreach ($data->getColNames() as $colName) {
                                        ?><th><?=$colName?></th><?php
                                    }
                                ?>
                                <th></th>
                            </tr>
                            <?php
                                $i = 1;
                                foreach ($data->getRecords() as $record) {
                                    ?><tr>
                                    <?php
                                        foreach ($data->getColValues($record, $i) as $colValue) {
                                            ?><td><?= $colValue ?></td><?php
                                        }
                                    ?>
                                        <td class="text-right"><button data-record-remove="<?=$record->getId()?>" data-record-type="<?=$recordType?> záznam" title="Odstrániť" class="button--remove">Zmazať</button></td>
                                    </tr><?php
                                    $i++;
                                }
                            ?>
                        </table>
                        <?php
                    }
                ?>
            </div>
    </body>
    <div data-popup class="popup">
        <div class="popup__container">
            <span data-popup-close class="popup__container__close" title="Zavrieť"></span>
            <div data-popup-title class="popup_title"></div>
            <div data-popup-content class="popup__content"></div>
            <div class="popup__action-container">
                <div>
                    <div data-loading="popup__loading--load" class="popup__loading">
                        <div></div>
                        <div></div>
                    </div>
                </div>
                <div>
                    <button data-button-cancel class="button button--cancel"></button>
                    <button data-button-confirm class="button button--confirm"></button>
                </div>
            </div>
        </div>
        <div data-popup-overlay class="popup__overlay"></div>
    </div>
    <div data-dynamic-popup-container></div>
</html>
