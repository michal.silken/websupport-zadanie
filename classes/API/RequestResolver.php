<?php


namespace classes\API;


use classes\API\requestTypes\RequestTypeInterface;

class RequestResolver
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var RequestTypeInterface[]
     */
    private $types = [];

    /**
     * RequestResolver constructor.
     * @param array $postdata
     */
    public function __construct(array $postdata)
    {
        $this->data = $postdata;
    }

    /**
     * @param RequestTypeInterface $requestType
     */
    public function add(RequestTypeInterface $requestType)
    {
        $this->types[] = $requestType;
    }

    /**
     * @return Response
     */
    public function resolve(): Response
    {
        foreach ($this->types as $type) {
            if ($type->match($this->data)) {
                $processor = $type->getProcessor($this->data);
                return $processor->getResponse();
            }
        }

        return new Response(false);
    }
}