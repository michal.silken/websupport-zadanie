<?php


namespace classes\API;


class Response implements \JsonSerializable
{
    /**
     * @var bool
     */
    private $status;

    /**
     * @var string
     */
    private $content;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * Response constructor.
     * @param bool $status
     * @param string $content
     * @param array $errors
     */
    public function __construct(bool $status, $content = '', $errors = [])
    {
        $this->status = $status;
        $this->content = $content;
        $this->errors = $errors;
    }

    public function jsonSerialize()
    {
        return [
            'status' => $this->status,
            'content' => $this->content,
            'errors' => $this->errors
        ];
    }
}