<?php

namespace classes\API\processors;

use classes\API\Response;


interface ProcessorInterface
{
    /**
     * @return Response
     */
    public function getResponse(): Response;
}