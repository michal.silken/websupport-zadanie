<?php


namespace classes\API\processors;


use classes\API\processors\validators\A;
use classes\API\processors\validators\AAAA;
use classes\API\processors\validators\ANAME;
use classes\API\processors\validators\CNAME;
use classes\API\processors\validators\MX;
use classes\API\processors\validators\NS;
use classes\API\processors\validators\SRV;
use classes\API\processors\validators\TXT;
use classes\API\processors\validators\ValidatorInterface;
use classes\API\Response;
use classes\websupport\api\Manager;

class Create implements ProcessorInterface
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var array
     */
    private $data;

    /**
     * @var ValidatorInterface[]
     */
    private static $validators = [
        'A' => A::class,
        'AAAA' => AAAA::class,
        'ANAME' => ANAME::class,
        'CNAME' => CNAME::class,
        'MX' => MX::class,
        'NS' => NS::class,
        'SRV' => SRV::class,
        'TXT' => TXT::class
    ];

    /**
     * Remove constructor.
     * @param Manager $manager
     * @param string $domain
     * @param array $postdata
     */
    public function __construct(Manager $manager, string $domain, array $postdata)
    {
        $this->manager = $manager;
        $this->domain = $domain;
        $this->data = $postdata;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        if (empty($this->data['type']) || empty(static::$validators[$this->data['type']])) {
            return new Response(false, 'Nedefinovaný typ záznamu!');
        }

        /**
         * @var $validator ValidatorInterface
         */
        $validator = new static::$validators[$this->data['type']]($this->data);
        if ($validator->validate()) {
            $result = $this->manager->create($this->domain, $validator);
            return new Response($result, '', $this->manager->getErrors());
        }
        return new Response(false, '', $validator->getErrors());
    }
}