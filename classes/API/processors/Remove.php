<?php


namespace classes\API\processors;


use classes\API\Response;
use classes\websupport\api\Manager;

class Remove implements ProcessorInterface
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var array
     */
    private $data;

    /**
     * Remove constructor.
     * @param Manager $manager
     * @param string $domain
     * @param array $postdata
     */
    public function __construct(Manager $manager, string $domain, array $postdata)
    {
        $this->manager = $manager;
        $this->domain = $domain;
        $this->data = $postdata;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        if (empty($this->data['removeId']) || !is_numeric($this->data['removeId'])) {
            return new Response(false);
        }

        $result = $this->manager->remove($this->domain, $this->data['removeId']);
        return new Response($result);
    }
}