<?php

namespace classes\API\processors;

use classes\API\Response;


class GetPopupTemplate implements ProcessorInterface
{
    /**
     * @var array
     */
    private $data;

    /**
     * GetPopupTemplate constructor.
     * @param array $postdata
     */
    public function __construct(array $postdata)
    {
        $this->data = $postdata;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        if (empty($this->data['getPopupTemplate'])) {
            return new Response(false);
        }

        $rootDir = dirname(dirname(dirname(__DIR__))) . '/views/popup/';
        $filename = substr(str_replace(['/', '.'], '', $this->data['getPopupTemplate']), -5).'.phtml';

        if (file_exists($rootDir . $filename)) {
            return new Response(
                true,
                file_get_contents($rootDir . $filename)
            );
        }

        return new Response(false);
    }
}