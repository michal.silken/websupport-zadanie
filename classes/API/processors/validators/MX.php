<?php


namespace classes\API\processors\validators;


class MX implements ValidatorInterface
{
    private static $fields = [
        'type',
        'name',
        'content',
        'prio',
        'ttl',
        'note'
    ];

    private $data;

    private $errors = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];
        foreach (static::$fields as $fieldName) {
            $data[$fieldName] = $this->data[$fieldName];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        foreach ($this->rules() as $key => $ruleCallback) {
            $ruleCallback($this->data[$key]);
        }

        return empty($this->errors);
    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'name' => function (&$value) {
                $result = !empty($value);
                if (!$result) {
                    $this->setError('name', 'Nemôže byť prázdne!');
                }
            },
            'content' => function (&$value) {
                $result = !empty($value);
                if (!$result) {
                    $this->setError('content', 'Nemôže byť prázdne!');
                }
            },
            'prio' => function (&$value) {
                $result = !empty($value) && is_numeric($value);
                if (!$result) {
                    $this->setError('prio', 'Musí byť čislo!');
                }
            },
            'ttl' => function (&$value) {
                $result = empty($value) || is_numeric($value);
                if (!$result) {
                    $this->setError('ttl', 'Musí byť čislo!');
                }
            },
        ];
    }

    /**
     * @param string $key
     * @param string $msg
     */
    private function setError(string $key, string $msg)
    {
        $this->errors[$key] = $msg;
    }
}