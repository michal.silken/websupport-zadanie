<?php


namespace classes\API\processors\validators;


interface ValidatorInterface
{
    /**
     * @return array
     */
    public function getData(): array;

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @return bool
     */
    public function validate(): bool;
}