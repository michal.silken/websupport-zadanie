<?php


namespace classes\API\processors\validators;


class A implements ValidatorInterface
{
    private static $IPv4Pattern = '/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/';

    private static $fields = [
        'type',
        'name',
        'content',
        'ttl',
        'note'
    ];

    private $data;

    private $errors = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];
        foreach (static::$fields as $fieldName) {
            $data[$fieldName] = $this->data[$fieldName];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        foreach ($this->rules() as $key => $ruleCallback) {
            $ruleCallback($this->data[$key]);
        }

        return empty($this->errors);
    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'name' => function (&$value) {
                $result = !empty($value);
                if (!$result) {
                    $this->setError('name', 'Nemôže byť prázdne!');
                }
            },
            'content' => function (&$value) {
                $result = !empty($value) && preg_match(static::$IPv4Pattern, $value);
                if (!$result) {
                    $this->setError('content', 'IP nieje validná!');
                }
            },
            'ttl' => function (&$value) {
                $result = empty($value) || is_numeric($value);
                if (!$result) {
                    $this->setError('ttl', 'Musí byť čislo!');
                }
            },
        ];
    }

    /**
     * @param string $key
     * @param string $msg
     */
    private function setError(string $key, string $msg)
    {
        $this->errors[$key] = $msg;
    }
}