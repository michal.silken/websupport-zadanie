<?php


namespace classes\API\requestTypes;


use classes\API\processors\ProcessorInterface;

class GetPopupTemplate implements RequestTypeInterface
{
    private static $fields = [
        'getPopupTemplate'
    ];

    /**
     * @param array $postdata
     * @return bool
     */
    public function match(array $postdata): bool
    {
        foreach (static::$fields as $fieldName) {
            if (isset($postdata[$fieldName])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $postdata
     * @return ProcessorInterface
     */
    public function getProcessor(array $postdata): ProcessorInterface
    {
        return new \classes\API\processors\GetPopupTemplate($postdata);
    }
}