<?php

namespace classes\API\requestTypes;

use classes\API\processors\ProcessorInterface;


interface RequestTypeInterface
{
    /**
     * @param array $postdata
     * @return bool
     */
    public function match(array $postdata): bool;

    /**
     * @param array $postdata
     * @return ProcessorInterface
     */
    public function getProcessor(array $postdata): ProcessorInterface;
}