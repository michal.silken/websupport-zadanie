<?php


namespace classes\API\requestTypes;


use classes\API\processors\ProcessorInterface;
use classes\websupport\api\Manager;

class Create implements RequestTypeInterface
{
    /**
     * @var array
     */
    private static $fields = [
        'create'
    ];

    /**
     * @var string
     */
    private $manager;

    /**
     * @var string
     */
    private $domain;

    /**
     * Remove constructor.
     * @param Manager $manager
     * @param string $domain
     */
    public function __construct(Manager $manager, string $domain)
    {
        $this->manager = $manager;
        $this->domain = $domain;
    }

    /**
     * @param array $postdata
     * @return bool
     */
    public function match(array $postdata): bool
    {
        foreach (static::$fields as $fieldName) {
            if (isset($postdata[$fieldName])) {
                return true;
            }
        }

        return false;
    }

    public function getProcessor(array $postdata): ProcessorInterface
    {
        return new \classes\API\processors\Create($this->manager, $this->domain, $postdata);
    }
}