<?php


namespace classes\websupport\api\dnsRecordTypes;


use classes\websupport\api\Record;

class ANAME extends AbstractType
{
    public function getColNames(): array
    {
        return [
            'Pre adresu',
            'Cieľová IP',
            'TTL',
            'POZNÁMKA'
        ];
    }

    public function getColValues(Record $record, $i = null): array
    {
        $name = $record->getName();
        return [
            ($name != '@' ? $name . '.' : '') . $this->domain,
            $record->getContent(),
            $record->getTTL(),
            $record->getNote()
        ];
    }
}