<?php


namespace classes\websupport\api\dnsRecordTypes;


use classes\websupport\api\Record;

class NS extends AbstractType
{
    public function getColNames(): array
    {
        return [
            '',
            'Hodnota'
        ];
    }

    public function getColValues(Record $record, $i = null): array
    {
        return [
            'NS ' . $i,
            $record->getContent()
        ];
    }
}