<?php


namespace classes\websupport\api\dnsRecordTypes;


use classes\websupport\api\Record;

class TXT extends AbstractType
{
    public function getColNames(): array
    {
        return [
            'Pre adresu',
            'Hodnota',
            'TTL',
            'POZNÁMKA'
        ];
    }

    public function getColValues(Record $record, $i = null): array
    {
        $name = $record->getName();
        return [
            ($name != '@' ? $name . '.' : '') . $this->domain,
            $record->getContent(),
            $record->getTTL(),
            $record->getNote()
        ];
    }
}