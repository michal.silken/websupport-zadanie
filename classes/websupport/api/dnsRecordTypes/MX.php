<?php


namespace classes\websupport\api\dnsRecordTypes;


use classes\websupport\api\Record;

class MX extends AbstractType
{
    public function getColNames(): array
    {
        return [
            'Pre adresu',
            'Mail služby',
            'Priorita',
            'TTL',
            'Poznámka'
        ];
    }

    public function getColValues(Record $record, $i = null): array
    {
        $name = $record->getName();
        return [
            ($name != '@' ? $name . '.' : '') . $this->domain,
            $record->getContent(),
            $record->getPrio(),
            $record->getTTL(),
            $record->getNote()
        ];
    }
}