<?php


namespace classes\websupport\api\dnsRecordTypes;

use classes\websupport\api\Record;

class AAAA extends AbstractType
{
    public function getColNames(): array
    {
        return [
            'Pre adresu',
            'Cieľová IP',
            'TTL',
            'Poznámka'
        ];
    }

    public function getColValues(Record $record, $i = null): array
    {
        $name = $record->getName();
        return [
            ($name != '@' ? $name . '.' : '') . $this->domain,
            $record->getContent(),
            $record->getTTL(),
            $record->getNote()
        ];
    }
}