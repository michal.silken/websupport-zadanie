<?php

namespace classes\websupport\api\dnsRecordTypes;

use classes\websupport\api\Record;

interface RecordTypeInterface
{
    /**
     * @param Record $record
     */
    public function add(Record $record);

    /**
     * @return Record[]
     */
    public function getRecords();

    public function getColNames(): array;

    public function getColValues(Record $record, $i = null): array;
}