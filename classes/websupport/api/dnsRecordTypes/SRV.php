<?php


namespace classes\websupport\api\dnsRecordTypes;


use classes\websupport\api\Record;

class SRV extends AbstractType
{
    public function getColNames(): array
    {
        return [
            'Pre adresu',
            'Adresa služby',
            'Port',
            'Priorita',
            'Váha',
            'TTL',
            'Poznámka'
        ];
    }

    public function getColValues(Record $record, $i = null): array
    {
        $name = $record->getName();
        return [
            ($name != '@' ? $name . '.' : '') . $this->domain,
            $record->getContent(),
            $record->getPort(),
            $record->getPrio(),
            $record->getWeight(),
            $record->getTTL(),
            $record->getNote()
        ];
    }
}