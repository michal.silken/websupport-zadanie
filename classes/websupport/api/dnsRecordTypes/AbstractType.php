<?php

namespace classes\websupport\api\dnsRecordTypes;

use classes\websupport\api\Record;


abstract class AbstractType implements RecordTypeInterface
{
    /**
     * @var string
     */
    protected $domain;

    /**
     * @var Record[]
     */
    protected $records = [];

    /**
     * AbstractType constructor.
     * @param string $domain
     */
    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }

    /**
     * @param Record $record
     */
    public function add(Record $record)
    {
        $this->records[] = $record;
    }

    /**
     * @return Record[]
     */
    public function getRecords()
    {
        return $this->records;
    }
}