<?php


namespace classes\websupport\api;


class Record
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $content;
    /**
     * @var string
     */
    private $note;
    /**
     * @var int|null
     */
    private $ttl;
    /**
     * @var null
     */
    private $prio;
    /**
     * @var null
     */
    private $weight;
    /**
     * @var null
     */
    private $port;

    /**
     * Record constructor.
     * @param int $id
     * @param string $type
     * @param string $name
     * @param string $content
     * @param string $note
     * @param int|null $ttl
     * @param null $prio
     * @param null $weight
     * @param null $port
     */
    public function __construct(int $id, string $type, string $name, string $content, string $note, int $ttl = null, $prio = null, $weight = null, $port = null)
    {
        $this->id = $id;
        $this->type = $type;
        $this->name = $name;
        $this->content = $content;
        $this->note = $note;
        $this->ttl = $ttl;
        $this->prio = $prio;
        $this->weight = $weight;
        $this->port = $port;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getNote(): string
    {
        return $this->note;
    }

    /**
     * @return int
     */
    public function getTTL(): int
    {
        return $this->ttl;
    }

    /**
     * @return int|null
     */
    public function getPrio()
    {
        return $this->prio;
    }

    /**
     * @return int|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return int|null
     */
    public function getPort()
    {
        return $this->port;
    }
}