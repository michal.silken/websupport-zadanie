<?php

namespace classes\websupport\api;


use classes\API\processors\validators\ValidatorInterface;
use classes\websupport\api\dnsRecordTypes\RecordTypeInterface;


class Manager
{
    /**
     * @var Connector
     */
    private $connector;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * Manager constructor.
     * @param Connector $connector
     */
    public function __construct(Connector $connector)
    {
        $this->connector = $connector;
    }

    /**
     * @param string $domain
     * @param ValidatorInterface $validator
     * @return bool
     */
    public function create(string $domain, ValidatorInterface $validator)
    {
        $response = $this->connector->create($domain, $validator->getData());
        if (empty($response)) {
            return false;
        }

        $data = json_decode($response);
        if (!empty($data->status) && $data->status === 'success') {
            return true;
        }

        if (!empty($data->errors)) {
            foreach ($data->errors as $key => $value) {
                $this->setError($key, current($value));
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getSupportedRecordTypes()
    {
        return [
            'A',
            'AAAA',
            'ANAME',
            'CNAME',
            'MX',
            'NS',
            'SRV',
            'TXT'
        ];
    }

    /**
     * @param string $domain
     * @param int $id
     * @return bool|string
     */
    public function remove(string $domain,  $id) {
        if (empty($id) && !is_numeric($id)) {
            return false;
        }

        $response = $this->connector->delete($domain, (int)$id);
        if (empty($response)) {
            return false;
        }

        $data = json_decode($response);
        if (!empty($data->status) && $data->status === 'success') {
            return true;
        }

        return false;
    }

    /**
     * @param string $domain
     * @return RecordTypeInterface[]
     */
    public function getListOfAllZones(string $domain)
    {
        $response = $this->connector->getListOfAllZones($domain);
        if (empty($response)) {
            return [];
        }

        $data = json_decode($response);
        if (empty($data->items)) {
            return [];
        }

        $result = [];
        foreach ($data->items as $item) {

            if (!isset($result[$item->type])) {
                $className = 'classes\websupport\api\dnsRecordTypes\\' . $item->type;
                if (!class_exists($className)) {
                    continue;
                }
                $result[$item->type] = new $className($domain);
            }

            $result[$item->type]->add(
                new Record(
                    $item->id,
                    $item->type,
                    $item->name,
                    $item->content,
                    $item->note ?? '',
                    $item->ttl ?? null,
                    $item->prio ?? null,
                    $item->weight ?? null,
                    $item->port ?? null
                )
            );
        }
        ksort($result);
        return $result;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    private function setError($key, $msg)
    {
        $this->errors[$key] = $msg;
    }
}