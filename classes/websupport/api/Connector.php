<?php

namespace classes\websupport\api;

use classes\websupport\api\entity\AbstractType;


class Connector
{
    const METHOD_GET = 'GET';

    const METHOD_DELETE = 'DELETE';

    const METHOD_POST = 'POST';

    /**
     * @var string
     */
    private $apiUrl = 'https://rest.websupport.sk';

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $secret;

    /**
     * Connector constructor.
     * @param string $apiKey
     * @param string $secret
     */
    public function __construct(string $apiKey, string $secret)
    {
        $this->apiKey = $apiKey;
        $this->secret = $secret;
    }

    /**
     * @param string $domain
     * @return bool|string
     */
    public function getListOfAllZones(string $domain)
    {
        $path = '/v1/user/self/zone/'.$domain.'/record';
        $ch = curl_init();
        $this->setCurlOpt($ch, static::METHOD_GET, $path);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @param string $domain
     * @param array $data
     * @return bool|string
     */
    public function create(string $domain, array $data)
    {
        $path = '/v1/user/self/zone/'.$domain.'/record';
        $ch = curl_init();
        $this->setCurlOpt($ch, static::METHOD_POST, $path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @param string $domain
     * @param int $id
     * @return bool|string
     */
    public function delete(string $domain, int $id)
    {
        $path = '/v1/user/self/zone/'.$domain.'/record/'.$id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, static::METHOD_DELETE);
        $this->setCurlOpt($ch, static::METHOD_DELETE, $path);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * @param $ch
     * @param string $method
     * @param string $path
     */
    private function setCurlOpt($ch, string $method, string $path)
    {
        $time = time();
        $canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
        $signature = hash_hmac('sha1', $canonicalRequest, $this->secret);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->apiKey.':'.$signature);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Accept: application/json',
            'Accept-Language: sk',
            'Date: ' . gmdate('Ymd\THis\Z', $time)
        ]);
    }
}